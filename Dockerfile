FROM eclipse-temurin:17-jdk-jammy as base
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:resolve
COPY src ./src

FROM base as dev1
CMD ["./mvnw","spring-boot:run","-Dspring-boot.run.profiles=mysql"]

FROM base as dev2
CMD ["./mvnw","spring-boot:run","-Dspring-boot.run.profiles=mysql","-Dspring-boot.run.jvmArguments='-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8000'"]

FROM base as build
RUN ./mvnw package

FROM eclipse-temurin:17-jre-jammy as production
ARG USER_ID=1001
ARG GROUP_ID=1001
ARG USR=javis
RUN groupadd $USR -g $GROUP_ID && useradd -g $USER_ID -s /bin/sh -m -u $USER_ID $USR
COPY --from=build /app/target/spring-petclinic-*.jar /spring-petclinic.jar
CMD ["java", "-Djava.security.egd=fils:/dev/./urandom", "-jar","spring-petclinic.jar"]
USER $USR
EXPOSE 8080

FROM production as production-mysql
CMD ["java", "-Djava.security.egd=fils:/dev/./urandom", "-jar", "spring-petclinic.jar", "--spring.profiles.active=mysql"]